package com.ticxo.demomodel.model;

import org.bukkit.util.EulerAngle;

import com.ticxo.demomodel.model.animation.attack.AnimationAttack;
import com.ticxo.demomodel.model.animation.attack.AnimationCast;
import com.ticxo.demomodel.model.animation.idle.AnimationIdle;
import com.ticxo.demomodel.model.animation.walk.AnimationWalk;
import com.ticxo.modelapi.api.additions.EntityModelPart;
import com.ticxo.modelapi.api.animation.AnimationMap;
import com.ticxo.modelapi.api.modeling.Bone;
import com.ticxo.modelapi.api.modeling.ModelBase;
import com.ticxo.modelapi.api.modeling.Offset;
import com.ticxo.modelapi.api.modeling.Part;
import com.ticxo.modelapi.api.modeling.SkeletonModel;

import us.fihgu.toolbox.item.DamageableItem;
import us.fihgu.toolbox.resourcepack.model.ModelAxis;

public class PhantomEmpress extends ModelBase{

	public PhantomEmpress() {
		super("demomodel:phantom_empress", "phantomempress");
		
		DamageableItem dItem = DamageableItem.DIAMOND_HOE;
		ModelAxis axis = ModelAxis.y;
		int t = 128;
		
		EntityModelPart head = createPart("head", dItem);
		head.addBox(6, 6, 6, -3, 0, -3, axis, 0, 0, 0, 0, t, 0, 0);
		head.addBox(7, 7, 7, -3.5, -0.5, -3.5, axis, 0, 0, 0, 0, t, 45, 0);
		addPart(new Part(head, new Offset(0, 2, 0), new EulerAngle(0, 0, 0)));
		Bone bHead = new Bone(head);
		
		EntityModelPart body = createPart("body", dItem);
		body.addBox(5, 4, 3.5, -2.5, -4, -1.75, axis, 0, 0, 0, 0, t, 0, 12, 5, 4, 3);
		addPart(new Part(body, new Offset(0, 2, 0), new EulerAngle(0, 0, 0)));
		Bone bBody = new Bone(body);
		
		EntityModelPart waist = createPart("waist", dItem);
		waist.addBox(4, 3, 3, -2, -3, -1.5, axis, 0, 0, 0, 0, t, 16, 12);
		addPart(new Part(waist, new Offset(0, -0.25, 0), new EulerAngle(0, 0, 0)));
		Bone bWaist = new Bone(waist);
		
		EntityModelPart hip = createPart("hip", dItem);
		hip.addBox(6, 4, 3.5, -3, -4, -1.75, axis, 0, 0, 0, 0, t, 30, 12, 6, 4, 3);
		addPart(new Part(hip, new Offset(0, -0.1875, 0), new EulerAngle(0, 0, 0)));
		Bone bHip = new Bone(hip);
		
		// Arms
		EntityModelPart rightArm = createPart("rightarm", dItem);
		rightArm.addBox(1.5, 6, 2, -0.75, -5, -1, axis, 0, 0, 0, 0, t, 0, 27, 1, 6, 2);
		addPart(new Part(rightArm, new Offset(-0.1875, -0.0625, 0), new EulerAngle(0, 0, 0)));
		Bone bRightArm = new Bone(rightArm);
		
		EntityModelPart rightForearm = createPart("rightforearm", dItem);
		rightForearm.addBox(3, 2, 3, -1.5, -2, -2.5, axis, 0, 0, 0, 0, t, 6, 27);
		rightForearm.addBox(4, 3, 4, -2, -5, -3, axis, 0, 0, 0, 0, t, 18, 27);
		rightForearm.addBox(1.5, 6, 2, -0.75, -6, -2, axis, 0, 0, 0, 0, t, 34, 27, 1, 6, 2);
		addPart(new Part(rightForearm, new Offset(0, -0.3125, -0.0625), new EulerAngle(0, 0, 0)));
		Bone bRightForearm = new Bone(rightForearm);
		
		EntityModelPart leftArm = createPart("leftarm", dItem);
		leftArm.addBox(1.5, 6, 2, -0.75, -5, -1, axis, 0, 0, 0, 0, t, 0, 19, 1, 6, 2);
		addPart(new Part(leftArm, new Offset(0.1875, -0.0625, 0), new EulerAngle(0, 0, 0)));
		Bone bLeftArm = new Bone(leftArm);
		
		EntityModelPart leftForearm = createPart("leftforearm", dItem);
		leftForearm.addBox(3, 2, 3, -1.5, -2, -2.5, axis, 0, 0, 0, 0, t, 6, 19);
		leftForearm.addBox(4, 3, 4, -2, -5, -3, axis, 0, 0, 0, 0, t, 18, 19);
		leftForearm.addBox(1.5, 6, 2, -0.75, -6, -2, axis, 0, 0, 0, 0, t, 34, 19, 1, 6, 2);
		addPart(new Part(leftForearm, new Offset(0, -0.3125, -0.0625), new EulerAngle(0, 0, 0)));
		Bone bLeftForearm = new Bone(leftForearm);
		
		// Legs
		EntityModelPart rightThigh = createPart("rightthigh", dItem);
		rightThigh.addBox(2, 4, 2, -1, -4, -1, axis, 0, 0, 0, 0, t, 16, 35);
		addPart(new Part(rightThigh, new Offset(-0.09375, -0.25, 0), new EulerAngle(0, 0, 0)));
		Bone bRightThigh = new Bone(rightThigh);
		
		EntityModelPart rightForeleg = createPart("rightforeleg", dItem);
		rightForeleg.addBox(2, 6, 2, -1, -6, 0, axis, 0, 0, 0, 0, t, 24, 35);
		addPart(new Part(rightForeleg, new Offset(0, -0.25, 0.0625), new EulerAngle(0, 0, 0)));
		Bone bRightForeleg = new Bone(rightForeleg);
		
		EntityModelPart leftThigh = createPart("leftthigh", dItem);
		leftThigh.addBox(2, 4, 2, -1, -4, -1, axis, 0, 0, 0, 0, t, 0, 35);
		addPart(new Part(leftThigh, new Offset(0.09375, -0.25, 0), new EulerAngle(0, 0, 0)));
		Bone bLeftThigh = new Bone(leftThigh);
		
		EntityModelPart leftForeleg = createPart("leftforeleg", dItem);
		leftForeleg.addBox(2, 6, 2, -1, -6, 0, axis, 0, 0, 0, 0, t, 8, 35);
		addPart(new Part(leftForeleg, new Offset(0, -0.25, 0.0625), new EulerAngle(0, 0, 0)));
		Bone bLeftForeleg = new Bone(leftForeleg);

		// Dress
		EntityModelPart upperDress = createPart("upperdress", dItem);
		upperDress.addBox(7, 5, 4, -3.5, -5, -2, axis, 0, 0, 0, 0, t, 0, 43);
		addPart(new Part(upperDress, new Offset(0, 0.015625, 0), new EulerAngle(0, 0, 0)));
		Bone bUpperDress = new Bone(upperDress);

		EntityModelPart middleDress = createPart("middledress", dItem);
		middleDress.addBox(8, 4, 4, -4, -4, 0, axis, 0, 0, 0, 0, t, 22, 43);
		addPart(new Part(middleDress, new Offset(0, -0.296875, 0.125), new EulerAngle(0, 0, 0)));
		Bone bMiddleDress = new Bone(middleDress);

		EntityModelPart lowerDress = createPart("lowerdress", dItem);
		lowerDress.addBox(9, 3, 4, -4.5, -3, 0, axis, 0, 0, 0, 0, t, 46, 43);
		lowerDress.addBox(8, 1, 3, -4, -4, 0.5, axis, 0, 0, 0, 0, t, 72, 43);
		addPart(new Part(lowerDress, new Offset(0, -0.25, 0), new EulerAngle(0, 0, 0)));
		Bone bLowerDress = new Bone(lowerDress);
		
		// Crown Dress
		EntityModelPart crownDress = createPart("crowndress", dItem);
		crownDress.addBox(16, 7, 12, -8, -4, -6, axis, 0, 0, 0, 0, t, 0, 52);
		addPart(new Part(crownDress, new Offset(0, 0.015625, 0), new EulerAngle(0, 0, 0)));
		Bone bCrownDress = new Bone(crownDress);
		
		bRightArm.addChild(bRightForearm);
		bLeftArm.addChild(bLeftForearm);
		
		bRightThigh.addChild(bRightForeleg);
		bLeftThigh.addChild(bLeftForeleg);
		bMiddleDress.addChild(bLowerDress);
		bUpperDress.addChild(bMiddleDress);
		
		bHip.addChild(bRightThigh);
		bHip.addChild(bLeftThigh);
		bHip.addChild(bUpperDress);
		bHip.addChild(bCrownDress);
		
		bWaist.addChild(bHip);
		bBody.addChild(bRightArm);
		bBody.addChild(bLeftArm);
		bBody.addChild(bWaist);
		
		SkeletonModel skeleton = new SkeletonModel(bHead, bBody);
		setSkeletonModel(skeleton);
		
		AnimationMap animation = new AnimationMap();
		animation.addNode("idle", new AnimationIdle(this));
		animation.addNode("walk", new AnimationWalk(this));
		animation.addNode("attack", new AnimationAttack());
		animation.addNode("cast", new AnimationCast());
		setAnimationMap(animation);
		
	}

}
