package com.ticxo.demomodel.model.animation.attack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.util.EulerAngle;

import com.ticxo.modelapi.api.animation.Animation;
import com.ticxo.modelapi.api.animation.preset.KeyFrame;
import com.ticxo.modelapi.api.animation.preset.Sequence;
import com.ticxo.modelapi.api.animation.preset.SequenceAnimation;
import com.ticxo.modelapi.api.modeling.Part;

public class AnimationAttack implements Animation{

	private Map<String, Animation> animation = new HashMap<String, Animation>();
	
	public AnimationAttack() {
		
		List<KeyFrame> rightArmKeys = new ArrayList<KeyFrame>();
		rightArmKeys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(180), Math.toRadians(90), Math.toRadians(-35))));
		rightArmKeys.add(new KeyFrame(5, new EulerAngle(Math.toRadians(-65), Math.toRadians(-55), Math.toRadians(40))));
		rightArmKeys.add(new KeyFrame(8, new EulerAngle(Math.toRadians(-20), Math.toRadians(-65), Math.toRadians(30))));
		animation.put("phantom_empress/rightarm", new SequenceAnimation(new Sequence(rightArmKeys)));
		
		List<KeyFrame> rightForearmKeys = new ArrayList<KeyFrame>();
		rightForearmKeys.add(new KeyFrame(0, new EulerAngle(0, 0, 0)));
		rightForearmKeys.add(new KeyFrame(5, new EulerAngle(0, 0, 0)));
		rightForearmKeys.add(new KeyFrame(8, new EulerAngle(Math.toRadians(-60), 0, 0)));
		animation.put("phantom_empress/rightforearm", new SequenceAnimation(new Sequence(rightForearmKeys)));
		
	}
	
	@Override
	public void entityParentConnection(Entity parent, ArmorStand target, Part part, EulerAngle head, EulerAngle body) {
		
		if(animation.containsKey(part.getModelName())) {
			animation.get(part.getModelName()).entityParentConnection(parent, target, part, head, body);
		}
		
	}

	@Override
	public void partParentConnection(ArmorStand parent, ArmorStand target, Part part, EulerAngle head, EulerAngle body) {
		
		if(animation.containsKey(part.getModelName())) {
			animation.get(part.getModelName()).partParentConnection(parent, target, part, head, body);
		}
		
	}

	@Override
	public Animation createAnimation() {

		return new AnimationAttack();
		
	}

	@Override
	public boolean containsPartAnimation(Part part) {
		return animation.containsKey(part.getModelName());
	}

}
