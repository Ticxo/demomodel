package com.ticxo.demomodel.model.animation.walk;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import com.ticxo.modelapi.api.animation.Animation;
import com.ticxo.modelapi.api.modeling.Offset;
import com.ticxo.modelapi.api.modeling.Part;
import com.ticxo.modelapi.math.Quaternion;

public class VelocityJoint implements Animation {

	private double mul = 1, vel;
	private EulerAngle offset;
	private String partType = "body";
	private Vector prevLoc = null, dir = null;
	
	public VelocityJoint(double mul, double vel, EulerAngle offset) {
		
		this.mul = mul;
		this.vel = vel;
		this.offset = offset;
		
	}
	
	public VelocityJoint(double mul, double vel, EulerAngle offset, String partType) {
		
		this.mul = mul;
		this.vel = vel;
		this.offset = offset;
		this.partType = partType;
		
	}
	
	@Override
	public void entityParentConnection(Entity parent, ArmorStand target, Part part, EulerAngle head, EulerAngle body) {

		updateDirection(target);
		
		Offset pos = part.getLocationOffset();
		
		Location l = parent.getLocation();
		l.setYaw(0);
		l.setPitch(0);
		l.add(pos.getX(), pos.getY() - 0.725, pos.getZ());
		
		target.teleport(l);
		EulerAngle angle;
		if(partType.toLowerCase().equals("head"))
			angle = Quaternion.combine(Quaternion.combine(part.getRotationOffset(), head), new EulerAngle(Math.toRadians(dir.getZ()), 0, Math.toRadians(dir.getX())));
		else
			angle = Quaternion.combine(Quaternion.combine(part.getRotationOffset(), body), new EulerAngle(Math.toRadians(dir.getZ()), 0, Math.toRadians(dir.getX())));
		target.setHeadPose(Quaternion.combine(offset, angle));
		
	}

	@Override
	public void partParentConnection(ArmorStand parent, ArmorStand target, Part part, EulerAngle head, EulerAngle body) {

		updateDirection(target);
		
		Offset pos = part.getLocationOffset();
		
		target.teleport(pos.getRelativeLocation(parent.getLocation(), parent.getHeadPose()));
		EulerAngle angle;
		if(partType.toLowerCase().equals("head"))
			angle = Quaternion.combine(Quaternion.combine(part.getRotationOffset(), head), new EulerAngle(Math.toRadians(dir.getZ()), 0, Math.toRadians(dir.getX())));
		else
			angle = Quaternion.combine(Quaternion.combine(part.getRotationOffset(), parent.getHeadPose()), new EulerAngle(Math.toRadians(dir.getZ()), 0, Math.toRadians(dir.getX())));
		target.setHeadPose(Quaternion.combine(offset, angle));
		
	}

	@Override
	public Animation createAnimation() {
		
		return new VelocityJoint(mul, vel, offset, partType);
		
	}

	@Override
	public boolean containsPartAnimation(Part part) {
		return true;
	}
	
	private void updateDirection(Entity ent) {
		
		double d = 0;
		
		if(prevLoc != null) {
			d = prevLoc.distance(ent.getLocation().toVector());
			dir = prevLoc.subtract(ent.getLocation().toVector()).normalize().multiply(-vel*clamp(d * mul, 0, 1));
		}else
			dir = new Vector(0, 0, 0);
		prevLoc = ent.getLocation().toVector();
	}
	
	private double clamp(double val, double min, double max) {
		return Math.max(min, Math.min(max, val));
	}

}
