package com.ticxo.demomodel.model.animation.idle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.util.EulerAngle;

import com.ticxo.modelapi.api.animation.Animation;
import com.ticxo.modelapi.api.animation.preset.KeyFrame;
import com.ticxo.modelapi.api.animation.preset.Sequence;
import com.ticxo.modelapi.api.animation.preset.SequenceAnimation;
import com.ticxo.modelapi.api.modeling.ModelBase;
import com.ticxo.modelapi.api.modeling.Offset;
import com.ticxo.modelapi.api.modeling.Part;

public class AnimationIdle implements Animation {

	private ModelBase model;
	private Map<String, Animation> animation = new HashMap<String, Animation>();
	
	public AnimationIdle(ModelBase model) {
		
		this.model = model;
		for(String partId : model.getParts().keySet())
			animation.put(partId, createSequence(partId));
		
	}
	
	@Override
	public void entityParentConnection(Entity parent, ArmorStand target, Part part, EulerAngle head, EulerAngle body) {
	
		animation.get(part.getModelName()).entityParentConnection(parent, target, part, head, body);
		
	}

	@Override
	public void partParentConnection(ArmorStand parent, ArmorStand target, Part part, EulerAngle head, EulerAngle body) {
		
		animation.get(part.getModelName()).partParentConnection(parent, target, part, head, body);
		
	}

	@Override
	public Animation createAnimation() {
		
		return new AnimationIdle(model);
		
	}

	@Override
	public boolean containsPartAnimation(Part part) {
		return animation.containsKey(part.getModelName());
	}
	
	private SequenceAnimation createSequence(String partId){
		
		List<KeyFrame> keys = new ArrayList<KeyFrame>();
		
		switch(partId.split("/")[1]) {
		case "head":
			keys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(10), 0, 0), new Offset(0, 0, 0)));
			keys.add(new KeyFrame(30, new EulerAngle(Math.toRadians(0), 0, 0), new Offset(0, -0.125, 0)));
			keys.add(new KeyFrame(60, new EulerAngle(Math.toRadians(10), 0, 0), new Offset(0, 0, 0)));
			break;
		case "body":
			keys.add(new KeyFrame(0, new Offset(0, 0, 0)));
			keys.add(new KeyFrame(30, new Offset(0, -0.125, 0)));
			keys.add(new KeyFrame(60, new Offset(0, 0, 0)));
			break;
		case "upperdress":
		case "middledress":
		case "lowerdress":
			keys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(5), 0, 0)));
			keys.add(new KeyFrame(30, new EulerAngle(Math.toRadians(10), 0, 0)));
			keys.add(new KeyFrame(60, new EulerAngle(Math.toRadians(5), 0, 0)));
			break;
		case "rightarm":
			keys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(-20), Math.toRadians(-65), Math.toRadians(30))));
			keys.add(new KeyFrame(30, new EulerAngle(Math.toRadians(-25), Math.toRadians(-65), Math.toRadians(40))));
			keys.add(new KeyFrame(60, new EulerAngle(Math.toRadians(-20), Math.toRadians(-65), Math.toRadians(30))));
			break;
		case "leftarm":
			keys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(-20), Math.toRadians(65), Math.toRadians(-30))));
			keys.add(new KeyFrame(30, new EulerAngle(Math.toRadians(-25), Math.toRadians(65), Math.toRadians(-40))));
			keys.add(new KeyFrame(60, new EulerAngle(Math.toRadians(-20), Math.toRadians(65), Math.toRadians(-30))));
			break;
		case "rightforearm":
		case "leftforearm":
			keys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(-60), 0, 0)));
			keys.add(new KeyFrame(30, new EulerAngle(Math.toRadians(-70), 0, 0)));
			keys.add(new KeyFrame(60, new EulerAngle(Math.toRadians(-60), 0, 0)));
			break;
		case "rightthigh":
			keys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(10), Math.toRadians(10), Math.toRadians(-5))));
			keys.add(new KeyFrame(20, new EulerAngle(Math.toRadians(10), Math.toRadians(10), Math.toRadians(-5))));
			break;
		case "leftthigh":
			keys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(-10), Math.toRadians(-10), Math.toRadians(5))));
			keys.add(new KeyFrame(20, new EulerAngle(Math.toRadians(-10), Math.toRadians(-10), Math.toRadians(5))));
			break;
		case "leftforeleg":
			keys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(15), 0, 0)));
			keys.add(new KeyFrame(20, new EulerAngle(Math.toRadians(15), 0, 0)));
			break;
		default:
			keys.add(new KeyFrame(0, new EulerAngle(0, 0, 0)));
			keys.add(new KeyFrame(20, new EulerAngle(0, 0, 0)));
			break;
		}
		
		if(partId.split("/")[1].toLowerCase().equals("head"))
			return new SequenceAnimation(new Sequence(keys), true);
		return new SequenceAnimation(new Sequence(keys));
				
	}

}
